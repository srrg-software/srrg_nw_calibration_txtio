# SRRG New World Calibration based on txtio #

This package allows to load a set of txtio based files and perform heterogeneous multi sensor calibration.

### Dependencies ###

* eigen3
* [srrg_core](https://gitlab.com/srrg-software/srrg_core)
* [srrg_nw_calibration](https://gitlab.com/srrg-software/srrg_nw_calibration)

### Installation ###
Standard ros package installation

    cd YOUR-ROS-PACKAGES
    git clone https://gitlab.com/srrg-software/srrg_nw_calibration_txtio.git
    cd ROS-WORKSPACE/src
    ln -s YOUR-ROS-PACKAGES/srrg_nw_calibration_txtio .
    cd ..
    catkin_make --pkg srrg_nw_calibration_txtio

### Test the package ###
The following command launches a full test with a stored dataset containing one laser and three asus Xtions. The output will be
composed by the three odometry parameters (kl, kr, baseline), the 2d pose of the laser and the set of 3d poses of the Xtions.

    cd srrg_nw_calibration_txtio/dataset/multi_orb
    rosrun srrg_nw_calibration_txtio new_world_calibration_txtio_app -rgb-topic /camera_left/rgb/image_raw -rgb-topic /camera_right/rgb/image_raw -rgb-topic /camera_front/rgb/image_raw -laser-topic /scan left.txt right.txt front.txt laser.txt

expected output (consider approximations)

    P [/base_link -> /odom]: -1.35766  1.35667 0.436661
    P [/base_link -> /thin_navigation_frame]: -0.00174767  0.00638352   0.0311065
    P [/base_link -> camera_left_rgb]: -0.0453427  0.0517742   0.259998  -0.660325   0.465724  -0.351811
    P [/base_link -> camera_right_rgb]: -0.0548018 -0.0987215   0.260062  -0.479599   0.654274   -0.47569
    P [/base_link -> camera_front_rgb]: -0.0568949  0.0186289   0.259996  -0.510943

