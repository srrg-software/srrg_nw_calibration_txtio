#include "srrg_messages/message_reader.h"
#include "srrg_messages/joint_state_message.h"
#include "srrg_messages/pinhole_image_message.h"
#include "srrg_messages/laser_message.h"
#include "srrg_messages/message_timestamp_synchronizer.h"
#include "srrg_system_utils/system_utils.h"

#include "srrg_nw_calibration_solver/multi_solver.hpp"

#include <list>


using namespace new_world_calibration;
using namespace srrg_core;


const char* banner[]={
    "\n\nUsage:  new_world_calibration_app -joint-topic <joint_topic> -rgb-topic <rgb_topic> -laser-topic <laser_topic> [Options] <dump_files.txt>\n",
    "Example:  new_world_calibration_app -joint-topic /joint_state -rgb-topic /camera_left/image_raw/rgb -laser-topic /scan left.txt laser.txt\n\n",
    "Options:\n",
    "------------------------------------------\n",
    "-joint-topic <string>       topic name of joints of the platform",
    "-rgb-topic <string>         topic name of a 3d sensor(e.g. camera)",
    "-laser-topic <string>       topic name of a 2d sensor(e.g. laser)",
    "-iterations <int>           required iterations for the least squares. default 100",
    "-tick-size <int>            size of the joint states to be red. default 2",
    "-o <string>                 output file of transforms. default no file is generated",
    "-base-link <string>         if an output file is required, the base_link frame can be changed here, default /base_link",
    "-h                          this help\n",
    0
};


int main(int argc, char ** argv){

    if(argc < 2) {
        printBanner(banner);
        return 1;
    }


    std::vector<std::string> rgb_topics;
    std::vector<std::string> rgb_frame_id;
    std::vector<std::string> laser_topics;
    std::vector<std::string> laser_frame_id;
    std::string joint_topic = "/joint_state";
    std::string base_link = "/base_link";
    std::string odom_frame = "/odom";
    std::string output_transform;
    output_transform = "";
    int iterations = 100;
    int sensor_num = 0;
    int tick_size = 2; //differential drive standard

    int c=1;
    std::vector<std::string> filenames;

    while(c<argc){
        if (! strcmp(argv[c],"-h")){
            printBanner(banner);
            return 1;
        }
        else if (! strcmp(argv[c],"-rgb-topic")){
            c++;
            rgb_topics.push_back(argv[c]);
        }
        else if(! strcmp(argv[c],"-joint-topic")){
            c++;
            joint_topic = argv[c];
        }
        else if(! strcmp(argv[c],"-iterations")){
            c++;
            iterations = std::atoi(argv[c]);
        }
        else if(! strcmp(argv[c],"-tick-size")){
            c++;
            iterations = std::atoi(argv[c]);
        }
        else if(! strcmp(argv[c],"-laser-topic")){
            c++;
            laser_topics.push_back(argv[c]);
        }
        else if(! strcmp(argv[c],"-o")){
            c++;
            output_transform = argv[c];
        }
        else if(! strcmp(argv[c],"-base-link")){
            c++;
            base_link = argv[c];
        }
        else{
            filenames.push_back(argv[c]);
        }
        c++;
    }

    for(std::vector<std::string>::iterator it = rgb_topics.begin(); it != rgb_topics.end(); ++it )
        std::cerr<<"rgb-topic i:     "<<(*it)<<std::endl;
    for(std::vector<std::string>::iterator it = laser_topics.begin(); it != laser_topics.end(); ++it )
        std::cerr<<"laser-topic i:   "<<(*it)<<std::endl;
    for(std::vector<std::string>::iterator it = filenames.begin(); it != filenames.end(); ++it )
        std::cerr<<"filename i:     "<<(*it)<<std::endl;
    std::cerr<<"joint-topic: "<<joint_topic<<std::endl;
    std::cerr<<"iterations:  "<<iterations<<std::endl;
    std::cerr<<"tick_size:  "<<tick_size<<std::endl;
    std::cerr<<"output_transform:  "<<output_transform<<std::endl;
    //    std::cerr<<"replicate:  "<<replicate<<std::endl;

    sensor_num = filenames.size();
    int so3_sensor_num = rgb_topics.size();
    int so2_sensor_num = laser_topics.size();


    if(sensor_num != (so2_sensor_num + so3_sensor_num))
    {
        std::cerr<<"different size between (rgb topics + laser topics) and filenames!"<<std::endl;
        std::exit(-1);
    }

    /// READING DATASET
    Dataset3Vector<float> calib_dat3f;
    Dataset2Vector<float> calib_dat2f;

    int current_file = 0;
    ///RGB, aka 3D, PART
    for(std::vector<std::string>::iterator it = rgb_topics.begin(); it != rgb_topics.end(); ++it, ++current_file){
        std::cerr<<"reading 3d dataset"<<std::endl;
        std::string curr_rgb_topic = (*it);
        std::string curr_file = filenames.at(current_file);

        Dataset3<float>* dataset_absolute = new Dataset3<float>;
        Dataset3<float>* dataset_relative = new Dataset3<float>;
        Sample3<float>* sample = new Sample3<float>;
        Sample3<float>* old_sample = new Sample3<float>;

        MessageReader reader;
        reader.open(curr_file);

        MessageTimestampSynchronizer synchronizer;
        synchronizer.setTimeInterval(0.001);
        std::vector<std::string> sync_topic;
        sync_topic.push_back(curr_rgb_topic);
        sync_topic.push_back(joint_topic);

        synchronizer.setTopics(sync_topic);

        BaseMessage *msg = 0;
        while(msg = reader.readMessage()) {
            BaseSensorMessage* sensor_msg = dynamic_cast<BaseSensorMessage*>(msg);
            if (! sensor_msg)
                delete msg;
            else {
                sensor_msg->untaint();
            }

            if (sensor_msg->topic() == curr_rgb_topic) {
                synchronizer.putMessage(sensor_msg);
                if(current_file == rgb_frame_id.size())
                    rgb_frame_id.push_back(sensor_msg->frameId());
            } else if (sensor_msg->topic() == joint_topic) {
                synchronizer.putMessage(sensor_msg);
            } else {
                delete sensor_msg;
            }

            if (synchronizer.messagesReady()) {
                //store data
                PinholeImageMessage* rgb_msg = dynamic_cast<PinholeImageMessage*>(synchronizer.messages()[0].get());
                JointStateMessage* joint_msg = dynamic_cast<JointStateMessage*>(synchronizer.messages()[1].get());
                if (!joint_msg){
                    rgb_msg = dynamic_cast<PinholeImageMessage*>(synchronizer.messages()[1].get());
                    joint_msg = dynamic_cast<JointStateMessage*>(synchronizer.messages()[0].get());
                }

                rgb_msg->untaint();

                joint_msg->untaint();


                sample->setTicks(Eigen::Vector2f(joint_msg->joints()[0].position, joint_msg->joints()[1].position));
                sample->setIsometry(rgb_msg->odometry());
                dataset_absolute->push_back(sample);

                if (dataset_relative->size() == 0) {
                    Sample3<float>* empty = new Sample3<float>(Eigen::Vector2f::Zero(), Eigen::Isometry3f::Identity());
                    dataset_relative->push_back(empty);
                } else {
                    Sample3<float>* relative = new Sample3<float>;
                    Eigen::Vector2f relative_ticks = sample->ticks() - old_sample->ticks();
                    normalizeTheta(relative_ticks);
                    relative->setTicks(relative_ticks);
                    Eigen::Isometry3f relative_isometry = old_sample->isometry().inverse() * sample->isometry();
                    relative->setIsometry(relative_isometry);
                    dataset_relative->push_back(relative);
                }
                *old_sample = *sample;

                synchronizer.reset();
            }

        }

        calib_dat3f.push_back(dataset_relative);

    }


    ///LASER, aka 2D, PART
    for(std::vector<std::string>::iterator it = laser_topics.begin(); it != laser_topics.end(); ++it, ++current_file){
        std::cerr<<"reading 2d dataset"<<std::endl;
        std::string curr_laser_topic = (*it);
        std::string curr_file = filenames.at(current_file);

        Dataset2<float>* dataset_absolute = new Dataset2<float>;
        Dataset2<float>* dataset_relative = new Dataset2<float>;
        Sample2<float>* sample = new Sample2<float>;
        Sample2<float>* old_sample = new Sample2<float>;

        MessageReader reader;
        reader.open(curr_file);

        MessageTimestampSynchronizer synchronizer;
        synchronizer.setTimeInterval(0.001);
        std::vector<std::string> sync_topic;
        sync_topic.push_back(curr_laser_topic);
        sync_topic.push_back(joint_topic);

        synchronizer.setTopics(sync_topic);

        BaseMessage *msg = 0;
        while(msg = reader.readMessage()) {
            BaseSensorMessage* sensor_msg = dynamic_cast<BaseSensorMessage*>(msg);
            if (! sensor_msg)
                delete msg;
            else {
                sensor_msg->untaint();
            }

            if (sensor_msg->topic() == curr_laser_topic) {
                synchronizer.putMessage(sensor_msg);
                if(current_file - calib_dat3f.size() == laser_frame_id.size())
                    laser_frame_id.push_back(sensor_msg->frameId());
            } else if (sensor_msg->topic() == joint_topic) {
                synchronizer.putMessage(sensor_msg);
            } else {
                delete sensor_msg;
            }

            if (synchronizer.messagesReady()) {
                //store data
                LaserMessage* laser_msg = dynamic_cast<LaserMessage*>(synchronizer.messages()[0].get());
                JointStateMessage* joint_msg = dynamic_cast<JointStateMessage*>(synchronizer.messages()[1].get());
                if (!joint_msg){
                    laser_msg = dynamic_cast<LaserMessage*>(synchronizer.messages()[1].get());
                    joint_msg = dynamic_cast<JointStateMessage*>(synchronizer.messages()[0].get());
                }

                laser_msg->untaint();
                joint_msg->untaint();


                sample->setTicks(Eigen::Vector2f(joint_msg->joints()[0].position, joint_msg->joints()[1].position));
                sample->setIsometry(srrg_core::iso2(laser_msg->odometry()));
                dataset_absolute->push_back(sample);

                if (dataset_relative->size() == 0) {
                    Sample2<float>* empty = new Sample2<float>(Eigen::Vector2f::Zero(), Eigen::Isometry2f::Identity());
                    dataset_relative->push_back(empty);
                } else {
                    Sample2<float>* relative = new Sample2<float>;
                    Eigen::Vector2f relative_ticks = sample->ticks() - old_sample->ticks();
                    normalizeTheta(relative_ticks);
                    relative->setTicks(relative_ticks);
                    Eigen::Isometry2f relative_isometry = old_sample->isometry().inverse() * sample->isometry();
                    relative->setIsometry(relative_isometry);
                    dataset_relative->push_back(relative);

                    //std::cerr<<"is "<<t2v(relative_isometry).transpose()<<" . " << relative_ticks.transpose()<<std::endl;
                }
                *old_sample = *sample;

   //             std::cerr<<"ol "<<t2v(old_sample->isometry()).transpose()<<std::endl;
     //           std::cerr<<"sa "<<t2v(sample->isometry()).transpose()<<std::endl<<std::endl;

                synchronizer.reset();
            }

        }

        calib_dat2f.push_back(dataset_relative);

    }

    std::cerr<<"dataset ready"<<std::endl;


    /// INIT THE ROBOT
    DifferentialDrive<float>* robot = new DifferentialDrive<float>();

    MultiSolver<float>* multisolver = new MultiSolver<float>(so2_sensor_num, so3_sensor_num);
    multisolver->setKinematics(robot);

    multisolver->setEpsilon(1e-7);
    multisolver->setIterations(iterations);


    /// INITIAL GUESS
    Vector6<float> guess3d = Vector6<float>::Zero();
    Eigen::Vector3f guess2d = Eigen::Vector3f::Zero();
    Eigen::Vector3f guess_odom = Eigen::Vector3f::Zero();


    float z_fixed = 0.26;
    guess3d << 0, 0, z_fixed, -0.47, 0.52, -0.51;
    guess2d << 0, 0, 0;
    guess_odom << 0.5, 0.5 ,0.2;
    Eigen::Isometry3f guess_3dsens = v2t((Vector6<float>)guess3d);
    Eigen::Isometry2f guess_2dsens = v2t((Eigen::Vector3f)guess2d);

    SensorParams2Vector<float> sensor2d_params;
    SensorParams3Vector<float> sensor3d_params;

    for(int i=0; i<so2_sensor_num; i++)
      {
        SensorParams2<float>* laser_params = new SensorParams2<float>(guess_2dsens, laser_frame_id.at(i), base_link);
        sensor2d_params.push_back(laser_params);
      }
    for(int i=0; i<so3_sensor_num; i++)
      {
        SensorParams3<float>* camera_params = new SensorParams3<float>(guess_3dsens, rgb_frame_id.at(i), base_link);
        sensor3d_params.push_back(camera_params);
      }

    OdomParams<float>* odom_params = new OdomParams<float>(guess_odom, odom_frame , base_link);

    multisolver->setSensorParams2(sensor2d_params);
    multisolver->setSensorParams3(sensor3d_params);
    multisolver->setOdomParams(odom_params);

    //add a weight of 100 on existing 2d sensors (more reliables in terms of precision)
    std::vector<int> information_weights_2d_sensor(so2_sensor_num, 100);
    multisolver->setInformationWeights2(information_weights_2d_sensor);

    multisolver->init();

    /// CALL THE SOLVER

    std::cerr<<"solver is ready"<<std::endl;

    multisolver->solve(calib_dat2f,calib_dat3f);

    return 0;
}

